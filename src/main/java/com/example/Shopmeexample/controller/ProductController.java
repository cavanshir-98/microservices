package com.example.Shopmeexample.controller;

import com.example.Shopmeexample.Repository.ProductService.ProductServices;
import com.example.Shopmeexample.model.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductServices productServices ;

    @GetMapping ("/getAll")
    public List<Products> getAllPRoducts(){
        return  productServices.getAllProducts();
    }
}
