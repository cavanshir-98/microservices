package com.example.Shopmeexample.controller;



import java.util.HashMap;

import com.example.Shopmeexample.service.UserServices.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import com.example.Shopmeexample.controller.RequestPojo.ApiResponse;
import com.example.Shopmeexample.model.User;
import com.example.Shopmeexample.service.*;

@RestController
@RequestMapping("api/signup")
public class SignUpController {
    @Autowired
    UserService userservice;
    @RequestMapping("user")
    public ResponseEntity<?> userLogin(@RequestBody HashMap<String,String> signupRequest) {
        try {
            //TODO validation has to add for client request
            User user = userservice.signUpUser(signupRequest);
            return  ResponseEntity.ok(user);
        }catch(Exception e ) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }
}
