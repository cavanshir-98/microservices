package com.example.Shopmeexample.controller;


import com.example.Shopmeexample.JWTConfiguration.ShoppingConfiguration;
import com.example.Shopmeexample.controller.RequestPojo.ApiResponse;
import com.example.Shopmeexample.model.AddToCart;
import com.example.Shopmeexample.service.CartService.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("api/addtocart")
public class AddToCartController {
//
    @Autowired(required = true)
    CartService cartService;

    @RequestMapping("addProduct")
    public ResponseEntity<?> addCartProtuct(@RequestBody HashMap<String, String> addCartRequest) {
       try{
           String keys[] ={"productId","userId","qty","price"};
          if (ShoppingConfiguration.validationWithHashMap(keys,addCartRequest)){
              long productId=Long.parseLong(addCartRequest.get("productId"));
              long userId =  Long.parseLong(addCartRequest.get("userId"));
              int qty =  Integer.parseInt(addCartRequest.get("qty"));
              double price = Double.parseDouble(addCartRequest.get("price"));
              List<AddToCart> obj = cartService.addCartbyUserIdAndProductId(productId,userId,qty,price);
              return ResponseEntity.ok(obj);
          }else {
              throw new Exception("Error");
          }

       }catch (Exception e){
           e.printStackTrace();
           return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(),""));
       }
    }
    @RequestMapping("updateQtyForCart")
    public ResponseEntity<?> updateQtyForCart(@RequestBody HashMap<String,String> addCartRequest) {
        try {
            String keys[] = {"cartId", "userId", "qty", "price"};
            if (ShoppingConfiguration.validationWithHashMap(keys, addCartRequest)) {

            }
            long cartId = Long.parseLong(addCartRequest.get("cartId"));
            long userId = Long.parseLong(addCartRequest.get("userId"));
            int qty = Integer.parseInt(addCartRequest.get("qty"));
            double price = Double.parseDouble(addCartRequest.get("price"));
            cartService.updateQtyByCartId(cartId, qty, price);
            List<AddToCart> obj = cartService.getCartByUserId(userId);
            return ResponseEntity.ok(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }
    @RequestMapping("removeProduct")
    public ResponseEntity<?> removeCartProtuctId(@RequestBody HashMap<String, String> removeCartProduct) {
        try {
            String keys[] = {"userId","cartId"};
            if(ShoppingConfiguration.validationWithHashMap(keys, removeCartProduct)) {

            }
            List<AddToCart> obj = cartService.removeCartByUserId(Long.parseLong(removeCartProduct.get("cartId")), Long.parseLong(removeCartProduct.get("userId")));
            return ResponseEntity.ok(obj);
        }catch(Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }
            @RequestMapping("getCarts")
            public ResponseEntity<?> getCartUserId (@RequestBody HashMap < String, String > getCartUserIdd){
                try {
                    String keys[] = {"userId"};
                    if(ShoppingConfiguration.validationWithHashMap(keys, getCartUserIdd)) {
                    }
                    List<AddToCart> obj = cartService.getCartByUserId(Long.parseLong(getCartUserIdd.get("userId")));
                    return ResponseEntity.ok(obj);
                }catch(Exception e) {
                    return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
                }
        }

    }