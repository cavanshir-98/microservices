package com.example.Shopmeexample.Repository.ProductService;

import com.example.Shopmeexample.Repository.ProductRepo;
import com.example.Shopmeexample.model.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServices {

    @Autowired
    ProductRepo productRepo;

    public List<Products> getAllProducts() {
        return productRepo.findAll();
    }
}
