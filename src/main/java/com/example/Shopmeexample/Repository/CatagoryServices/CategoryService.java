package com.example.Shopmeexample.Repository.CatagoryServices;

import com.example.Shopmeexample.Repository.CategoryRepo;
import com.example.Shopmeexample.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    @Autowired
    CategoryRepo categoryRepo;


    public List<Category> getfull(){
        return categoryRepo.findAll();
    }
}
