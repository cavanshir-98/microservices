package com.example.Shopmeexample.Repository;

import com.example.Shopmeexample.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


public interface CategoryRepo extends JpaRepository<Category,Long> {
}
