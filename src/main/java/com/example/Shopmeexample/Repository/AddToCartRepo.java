package com.example.Shopmeexample.Repository;

import com.example.Shopmeexample.model.AddToCart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface AddToCartRepo extends JpaRepository<AddToCart,Long> {

     @Query("Select addCart FROM AddtoCart addCart WHERE addCart.user_id=:user_id")
     List<AddToCart> getCartByuserId(@Param("user_id")Long user_id);
     @Query("Select addCart FROM AddtoCart addCart WHERE addCart.product_id=:product_id and addCart.user_id=:user.id")
     Optional<AddToCart> getCartByProductuserId(@Param("user_id")Long user_id,@Param("product_id")Long product_id);
     @Modifying
     @Transactional
     @Query("DELETE FROM AddtoCart d WHERE addCart.id=:cart_id and addCart.user_id=:user_id")
     void deleteCartByIdUserid(@Param("user_id")Long user_id,@Param("cart_id")Long cart_id);

     @Modifying
     @Transactional
     @Query("DELETE FROM AddtoCart d WHERE d.user_id=:user_id")
     void deleteAllUserid(@Param("user_id")Long user_id);

     Optional<Object> getCartByProductIdAnduserId(long userId, long productId);

     void updateQtyByCartId(long cartId, double price, int qty);
}
