package com.example.Shopmeexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopmeexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShopmeexampleApplication.class, args);
	}

}
