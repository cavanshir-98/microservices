package com.example.Shopmeexample.service.CartService;

import com.example.Shopmeexample.model.AddToCart;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CartService {

    List<AddToCart> addCartbyUserIdAndProductId(long productId,long userId,int qty,double price) throws Exception;

   void updateQtyByCartId(long cartId,int qty,double price) throws Exception;

    List<AddToCart> getCartByUserId(long userId);

    List<AddToCart> removeCartByUserId(long cartId,long userId);

}
