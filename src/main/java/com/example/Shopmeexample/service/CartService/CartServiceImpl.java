package com.example.Shopmeexample.service.CartService;

import com.example.Shopmeexample.Repository.AddToCartRepo;
import com.example.Shopmeexample.model.AddToCart;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CartServiceImpl implements CartService {


    AddToCartRepo addToCartRepo ;
    @Override
    public List<AddToCart> addCartbyUserIdAndProductId(long productId, long userId,int qty,double price) throws Exception{

        try {
           if( addToCartRepo.getCartByProductIdAnduserId(userId,productId).isPresent()){
               throw new Exception("Product hal hazirda var");
           }
            AddToCart obj = new AddToCart();
            obj.setQty(qty);
            obj.setUserId(  userId);
            obj.setProduct(productId);
            obj.setPrice(price);
            addToCartRepo.save(obj);

        return this.getCartByUserId(userId);
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public void updateQtyByCartId(long cartId, int qty, double price) throws Exception {
        addToCartRepo.updateQtyByCartId(cartId,price,qty);
    }

    @Override
    public List<AddToCart> getCartByUserId(long userId) {
       return addToCartRepo.getCartByuserId(userId);
    }

    @Override
    public List<AddToCart> removeCartByUserId(long cartId, long userId) {

        return this.getCartByUserId(userId);
    }
}
